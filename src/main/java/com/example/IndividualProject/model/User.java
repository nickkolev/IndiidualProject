package com.example.IndividualProject.model;

public class User {

    private String name;
    private String userType; //{normal, subscriber, admin}
    private Country country;
    private int userID;
    private int age;

    public User(String name, String userType, Country country, int userID, int age) {
        this.name = name;
        this.userType = userType;
        this.country = country;
        this.userID = userID;
        this.age = age;
    }

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format("User: %s%n" +
                "User ID: %d%n" +
                "Age: %d%n" +
                "Country: %s%n", name, userID, age, country);
    }
}
