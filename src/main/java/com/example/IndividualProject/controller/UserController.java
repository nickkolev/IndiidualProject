package com.example.IndividualProject.controller;

import com.example.IndividualProject.model.*;
import com.example.IndividualProject.repository.FakeData;

import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {

    private static final FakeData dataStore = new FakeData();

    @GetMapping("{id}")
    public ResponseEntity<User> getUserPath(@PathVariable(value = "id") int id) {
        User user = dataStore.getUser(id);

        if(user != null) {
            return ResponseEntity.ok().body(user);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping
    public ResponseEntity<List<User>> getAllUsers(@RequestParam(value = "country") Optional<String> country) {
        List<User> users = null;

        if(country.isPresent()) {
            Country c = dataStore.getCountry(country.get());
            users = dataStore.getUsers(c);
        }
        else
        {
            users = dataStore.getUsers();
        }

        if(users != null) {
            return ResponseEntity.ok().body(users);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    /*
    @GetMapping
    public ResponseEntity<List<User>> getAllUsersUserType(@RequestParam(value = "userType") String userType) {
        List<User> users = null;
        if (userType.equals("Subscriber") || userType.equals("Normal") || userType.equals("Admin")) {
            users = dataStore.getUsers(userType);
        } else {
            users = dataStore.getUsers();
        }

        if(users != null) {
            return ResponseEntity.ok().body(users);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
     */

    @DeleteMapping("{id}")
    public ResponseEntity deletePost(@PathVariable int id) {
        dataStore.deleteUser(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping()
    public ResponseEntity<User> createUser(@RequestBody User user) {
        if (!dataStore.createUser(user)){
            String entity =  "Student with student number " + user.getUserID() + " already exists.";
            return new ResponseEntity(entity,HttpStatus.CONFLICT);
        } else {
            String url = "student" + "/" + user.getUserID(); // url of the created student
            URI uri = URI.create(url);
            return new ResponseEntity(uri,HttpStatus.CREATED);
        }
    }

    @PutMapping()
    public ResponseEntity<User> updateUser(@RequestBody User user) {
        if (dataStore.updateUser(user)) {
            return ResponseEntity.noContent().build();
        } else {
            return new ResponseEntity("Please provide a valid student number.",HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<User> updateUser(@PathVariable("id") int userID,
                                           @RequestParam("name") String name,
                                           @RequestParam("country") String countryCode,
                                           @RequestParam("userType") String userType,
                                           @RequestParam("age") int age) {
        User user = dataStore.getUser(userID);
        if (user == null){
            return new ResponseEntity("Please provide a valid student number.",HttpStatus.NOT_FOUND);
        }

        Country country = dataStore.getCountry(countryCode);
        if (country == null){
            return new ResponseEntity("Please provide a valid country code.",HttpStatus.BAD_REQUEST);
        }

        user.setUserType(userType);
        user.setAge(age);
        user.setName(name);
        user.setCountry(country);
        return ResponseEntity.noContent().build();
    }
}
