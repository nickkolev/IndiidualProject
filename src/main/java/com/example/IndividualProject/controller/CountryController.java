package com.example.IndividualProject.controller;

import com.example.IndividualProject.model.*;
import com.example.IndividualProject.repository.FakeData;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/country")
public class CountryController {
    private static final FakeData dataStore = new FakeData();

    @GetMapping("{code}")
    public ResponseEntity<Country> getCountryPath(@PathVariable(value = "code") String code) {
        Country country = dataStore.getCountry(code);

        if(country != null) {
            return ResponseEntity.ok().body(country);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping
    public ResponseEntity<List<Country>> getAllCountries(@RequestParam(value = "country") Optional<String> countryCode) {
        List<Country> countries = null;

        if(countryCode.isPresent()) {
            countries = dataStore.getAllCountries()
                    .stream()
                    .filter(x -> x.getCountryCode().equals(countryCode.get()))
                    .collect(Collectors.toList());
        } else {
            countries = dataStore.getAllCountries();
        }

        if(countries != null) {
            return ResponseEntity.ok().body(countries);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping()
    public ResponseEntity<Country> createCountry(@RequestBody Country country) {
        if (!dataStore.add(country)){
            String entity =  "Country with country code " + country.getCountryCode() + " already exists.";
            return new ResponseEntity(entity, HttpStatus.CONFLICT);
        } else {
            String url = "country" + "/" + country.getCountryCode();
            URI uri = URI.create(url);
            return new ResponseEntity(uri,HttpStatus.CREATED);
        }
    }

    @PutMapping()
    public ResponseEntity<Country> updateCountry(@RequestBody Country country) {
        if (dataStore.updateCountry(country)) {
            return ResponseEntity.noContent().build();
        } else {
            return new ResponseEntity("Please provide a valid country code.",HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("{code}")
    public ResponseEntity deleteCountry(@PathVariable String code) {
        dataStore.deleteCountry(code);
        return ResponseEntity.ok().build();

    }
}
