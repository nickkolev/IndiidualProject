package com.example.IndividualProject.repository;

import com.example.IndividualProject.model.Country;
import com.example.IndividualProject.model.User;

import java.util.List;
import java.util.ArrayList;

public class FakeData {

    private final List<User> userList = new ArrayList<>();
    private final List<Country> countryList = new ArrayList<>();

    public FakeData() {
        Country netherlands = new Country("The Netherlands", "NL", true);
        Country bulgaria = new Country("Bulgaria", "BG", true);
        Country china = new Country("China", "CNH", false);
        Country germany = new Country("Germany", "GR", true);
        Country poland = new Country("Poland", "PL", true);
        Country australia = new Country("Australia", "AU", false);
        Country usa = new Country("America", "USA", false);

        countryList.add(netherlands);
        countryList.add(bulgaria);
        countryList.add(china);
        countryList.add(germany);
        countryList.add(poland);
        countryList.add(australia);
        countryList.add(usa);

        userList.add(new User("Joe Kovacs", "Subscriber", usa, 1, 27));
        userList.add(new User("Sean Donnoley", "Subscriber", usa, 2, 30));
        userList.add(new User("Ann Johnsonn", "Subscriber", usa, 3, 51));
        userList.add(new User("Shu Wang", "Normal", china, 4, 44));
        userList.add(new User("Albert Huffman", "Subscriber", germany, 5, 36));
        userList.add(new User("Khaang Wing", "Normal", china, 6, 21));
        userList.add(new User("Ashton John", "Normal", australia, 7, 18));
        userList.add(new User("Nikola Kolev", "Admin", bulgaria, 9999, 20));
    }

    // GET all users
    public List<User> getUsers() {
        return userList;
    }

    // GET all users from a given country
    public List<User> getUsers(Country country) {
        List<User> filtered = new ArrayList<>();
        for (User user : userList) {
            if (user.getCountry().equals(country)) {
                filtered.add(user);
            }
        }
        return filtered;
    }

    // GET all users with a given userType
    public List<User> getUsers(String userType) {
        List<User> filtered = new ArrayList<>();
        for (User user : userList) {
            if (user.getUserType().equals(userType)) {
                filtered.add(user);
            }
        }
        return filtered;
    }

    // GET the user with the given ID
    public User getUser(int id) {
        for (User user : userList) {
            if (user.getUserID() == id) {
                return user;
            }
        }
        return null;
    }

    // GET all the countries
    public List<Country> getAllCountries() {
        return countryList;
    }

    // DELETE a user with the given ID
    public void deleteUser(int id) {
        User user = getUser(id);
        if (user == null) {
            return;
        }
        userList.remove(user);
    }

    // CREATE a new user
    public boolean createUser(User user) {
        if (this.getUser(user.getUserID()) != null){
            return false;
        }
        userList.add(user);
        return true;
    }

    // PUT - Update an existing user
    public boolean updateUser(User user) {
        User old = this.getUser(user.getUserID());
        if (old == null) {
            return false;
        }
        old.setName(user.getName());
        old.setCountry(user.getCountry());
        return true;
    }

    // GET country with an given code
    public Country getCountry(String countryCode) {
        for (Country country : countryList) {
            if (country.getCountryCode().equals(countryCode)) {
                return country;
            }
        }
        return null;
    }

    // DELETE country with a given country code
    public void deleteCountry(String countryCode) {
        Country country = getCountry(countryCode);
        if (country == null){
            return;
        }

        countryList.remove(country);
    }

    // Add country
    public boolean add(Country country) {
        if (this.getCountry(country.getCountryCode()) != null){
            return false;
        }
        countryList.add(country);
        return true;
    }

    // Update country
    public boolean updateCountry(Country country) {
        Country old = this.getCountry(country.getCountryCode());
        if (old == null) {
            return false;
        }
        old.setCountryName(country.getCountryName());
        old.setCountryCode(country.getCountryCode());
        return true;
    }
}
